<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ShopController extends Controller
{
    public function getAllProduct()
    {
        $products = Product::all();
        return view('shop', ['products' => $products]);
    }
    public function detailProduct($id)
    {
        $product = Product::find($id);
        return view('detailProduct', ['product' => $product]);
    }
}
