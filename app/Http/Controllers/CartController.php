<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function Cart()
    {
        return view('cart');
    }

    public function addToCart(Request $request)
    {
        $id = $request->id;
        $qty = $request->qty;
        $product = Product::find($id);
        $cart = Session()->get('cart');


        if (isset($cart[$id])) {
            $new_qty = $qty + $cart[$id]["quantity"];
            if ($new_qty > $product->quantity) {
                session()->flash('error', 'Vượt quá số lượng mua');
            } else {
                $cart[$id]['quantity'] = $new_qty;
                session()->put('cart', $cart);
                return redirect()->route('Cart');
            }
        } else {
            $cart[$id] = [
                "id" => $id,
                "name" => $product->name,
                "quantity" => $qty,
                "price" => $product->price

            ];
            session()->put('cart', $cart);
            return redirect()->route('Cart');
        }
    }
    public function deleteItem($id)
    {
        $cart = session()->get('cart');
        if (isset($cart[$id])) {

            unset($cart[$id]);

            session()->put('cart', $cart);
        }
    }
    public function deleteCart()
    {
        session()->flush('cart');
    }
}
