<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::post('/register', [AuthController::class, 'register'])->name('register');

Route::get('/viewLogin', function () {
    return view('login');
})->name('viewLogin');
Route::get('/viewRegister', function () {
    return view('register');
});
Route::get('/', [ShopController::class, 'getAllProduct'])->name('getAllProduct');
Route::get('detail/{id}', [shopController::class, 'detailProduct'])->name('detailProduct');

Route::post('addToCart', [CartController::class, 'addToCart'])->name('addToCart');
Route::get('deleteCart', [CartController::class, 'deleteCart'])->name('deleteCart');
Route::get('deleteItem/{id}', [CartController::class, 'deleteItem'])->name('deleteItem');
Route::get('cart', [CartController::class, 'Cart'])->name('Cart');
