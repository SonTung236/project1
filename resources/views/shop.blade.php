</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div>
        @if (Auth::check())
            Người dùng: {{ Auth::user()->name }}
            <a href="/logout">Đăng xuất</a>
        @else
            <a href="/viewLogin">Đăng nhập</a>/<a href="/viewRegister">Đăng ký</a>
        @endif
        <a href="/cart">Giỏ hàng</a>
    </div>
    <div class="container-fluid">
        <div class="row">
            @foreach ($products as $product)
                <div class="col-sm-4">
                    <form action="{{ route('addToCart') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $product->id }}">
                        <p>{{ $product->name }} {{ $product->quantity }} sản phẩm </p>
                        <p>{{ $product->category->name }}</p>
                        <p>giá:{{ number_format($product->price, 0, '', ',') }} </p>
                        <a href="{{ route('detailProduct', [$product->id]) }}">Chi tiết</a>
                        <input type="number" name="qty" value="1" min="1" max="{{ $product->quantity }}">
                        @if ($product->stock_status == 'còn hàng')
                            <button type="submit">Mua ngay</button>
                        @else
                            <button type="disable">Hết hàng</button>
                        @endif
                    </form>
                </div>
            @endforeach
        </div>
    </div>

</body>

</html>
