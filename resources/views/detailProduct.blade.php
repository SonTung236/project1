</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div>
        @if (Auth::check())
            Người dùng: {{ Auth::user()->name }}
            <a href="/logout">Đăng xuất</a>
        @else
            <a href="/viewLogin">Đăng nhập</a>/<a href="/viewRegister">Đăng ký</a>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
                <div class="col-sm-4">
                    <p>{{ $product->name }} {{ $product->quantity }} sản phẩm </p>
                    <p>{{ $product->category->name }}</p>
                    <p>giá:{{ number_format($product->price, 0, '', ',') }} </p>
                    <p>mô tả:{{ $product->decription}} </p></p>
                    <p>số lượng {{$product->quantity}} </p>
                    @if ($product->stock_status == 'còn hàng')
                        <a href="">mua ngay</a>
                    @else
                        <a href="" disabled>hết hàng</a>
                    @endif
                </div>
        
        </div>
    </div>

</body>

</html>
