</html>
<!DOCTYPE html>
<html>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }

</style>

<body>
    @if (session('cart'))
        <h2>Giỏ hàng</h2>

        <table style="width:100%">
            <tr>
                <th>Tên sản phẩm</th>
                <th>Giá</th>
                <th>Số lượng</th>
                <th>Thành tiền</th>
                <th></th>
            </tr>
            @foreach (session('cart') as $id => $details)
                <tr>
                    <?php $total = $details['quantity'] * $details['price']; ?>
                    <td>{{ $details['name'] }}</td>
                    <td>{{ number_format($details['price'], 0, '', ',') }}</td>
                    <td>{{ $details['quantity'] }}</td>
                    <td>{{ number_format($total, 0, '', ',') }}</td>
                    <td><a href="{{ route('deleteItem', $details['id']) }}">X </a></td>
                </tr>
            @endforeach
        </table>
        <a href="{{ route('deleteCart') }}">Xóa giỏ hàng<a>
            @else
                <h2>Giỏ hàng trống</h2>
    @endif


</body>

</html>
