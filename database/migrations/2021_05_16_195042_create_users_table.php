<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('Id');
            $table->string('Name');
            $table->string('Address');
            $table->date('Birthday');
            $table->mediumText('Avatar');
            $table->boolean('Gender');
            $table->string('Email')->unique();
            $table->string('PhoneNumber')->unique();
            $table->timestamp('Email_verified_at')->nullable();
            $table->string('Password');
            $table->unsignedBigInteger('UsersRoleId');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('UsersRoleId')->references('Id')->on('usersroleid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
